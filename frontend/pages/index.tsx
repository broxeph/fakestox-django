import {NextPage} from 'next';

const Home: NextPage<{ userAgent: string }> = ({userAgent}) => (
  <div>
    <h1>Hello world!</h1>
    <li>user agent: {userAgent}</li>

    <style jsx>{`
      h1 {
	      font-size: 3em;
	    }
	  `}</style>
  </div>
);

Home.getInitialProps = async ({req}) => {
  const userAgent = req ? req.headers['user-agent'] || '' : navigator.userAgent;
  return {userAgent};
};

export default Home;
