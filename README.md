# FakeStoxDjango

Simulate stock trades! Now, with 100% less risk!

Homepage: https://gitlab.com/aball/fakestox-django/

Elixir/Phoenix fork: https://gitlab.com/broxeph/fakestox/

*Disclaimer: I don't actually know anything about Docker nor React. This is going to be ugly...*

## Run the Trap

`docker-compose up --build`

In debug mode, a superuser is created automatically with username `admin` and password `password`.

## Stack

- Backend
    - Python 3.8
    - Django 3.0
    - Docker
    - Django Rest Framework
    - Postgres
- Frontend
    - React
    - Next.js
    - Axios (?)
    - Cypress (?)
- CI
    - GitLab CI
- CD
    - GitLab CD (?)
- Infra
    - Provisioning: Terraform (?)
    - Frontend: S3 / CloudFront (?)
    - Backend: AWS Elastic Beanstalk (?)

## Useful Commands

- Django shell
    - `docker exec -it fakestox_backend_1 bash -c "cd backend && bash"`
- Postgres shell
    - `docker exec -it fakestox_db_1 psql -U postgres`
- API smoke test
    - `curl -H 'Accept: application/json; indent=4' http://127.0.0.1:8000/v1/users/`

## Resources

- https://www.caktusgroup.com/blog/2017/03/14/production-ready-dockerfile-your-python-django-app/
