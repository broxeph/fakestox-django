#!/bin/bash

set -e  # Abort startup if migrations fail

cd backend
python manage.py migrate --no-input
python manage.py runserver 0.0.0.0:8000
