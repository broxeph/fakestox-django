from django.contrib.auth.models import Group
from rest_framework.serializers import HyperlinkedModelSerializer

from fakestox.models import User, Symbol, Trade, Transfer


class UserSerializer(HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'groups', 'cash', 'holdings_value', 'portfolio_value']


class GroupSerializer(HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'name']


class SymbolSerializer(HyperlinkedModelSerializer):
    class Meta:
        model = Symbol
        fields = ['url', 'symbol', 'name', 'price']


class TradeSerializer(HyperlinkedModelSerializer):
    class Meta:
        model = Trade
        fields = ['url', 'symbol', 'price', 'user', 'ts']


class TransferSerializer(HyperlinkedModelSerializer):
    class Meta:
        model = Transfer
        fields = ['url', 'amount', 'user', 'ts']
