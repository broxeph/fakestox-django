from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from rest_framework.viewsets import ModelViewSet

from fakestox.models import Symbol, Trade, Transfer
from fakestox.serializers import UserSerializer, GroupSerializer, SymbolSerializer, TradeSerializer, TransferSerializer


class UserViewSet(ModelViewSet):
    queryset = get_user_model().objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class GroupViewSet(ModelViewSet):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer


class SymbolViewSet(ModelViewSet):
    queryset = Symbol.objects.all()
    serializer_class = SymbolSerializer


class TradeViewSet(ModelViewSet):
    queryset = Trade.objects.all()
    serializer_class = TradeSerializer


class TransferViewSet(ModelViewSet):
    queryset = Transfer.objects.all()
    serializer_class = TransferSerializer
